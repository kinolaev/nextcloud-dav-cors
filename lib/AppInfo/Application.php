<?php

declare(strict_types=1);

namespace OCA\DAV_CORS\AppInfo;

use OCP\AppFramework\App;
use OCP\EventDispatcher\IEventDispatcher;
use OCP\SabrePluginEvent;
use OCA\DAV\Events\SabrePluginAddEvent;
use OCA\DAV_CORS\Connector\Sabre\CorsPlugin;

class Application extends App {

  public function __construct() {
    parent::__construct('dav_cors');

    $dispatcher = $this->getContainer()->query(IEventDispatcher::class);
    $dispatcher->addListener(SabrePluginAddEvent::class, function(SabrePluginAddEvent $event) {
      $event->getServer()->addPlugin(new CorsPlugin(\OC::$server->getConfig()));
    });
  }
}
